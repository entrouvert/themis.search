from five import grok
from zope import interface, schema, component
from zope.interface import implements
from Products.Five import BrowserView
from Products.CMFCore.utils import getToolByName
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from z3c.form import form, field, button, widget
from z3c.form.ptcompat import ViewPageTemplateFile
from z3c.form.browser.radio import RadioWidget
from z3c.form.browser.text import TextWidget
from z3c.form.interfaces import ITextWidget
import z3c.form.interfaces

from themis.fields.vocabs import get_terms_for_persons

from interfaces import MessageFactory as _

def FieldRadioboxesWidget(field, request):
    return widget.FieldWidget(field, RadioWidget(request))

class IContactWidget(ITextWidget):
    pass

class ContactWidget(TextWidget):
    implements(IContactWidget)
    input_template = ViewPageTemplateFile('contact-input.pt')

    def render(self):
        if self.mode == z3c.form.interfaces.INPUT_MODE:
            return self.input_template(self)
        return TextWidget.render(self)

    def get_known_contacts(self):
        terms = get_terms_for_persons(self.context, include_deputies=True,
                        include_ministries=True)
        return terms


def FieldContactWidget(field, request):
    return widget.FieldWidget(field, ContactWidget(request))

@grok.provider(IContextSourceBinder)
def possible_scopes(context):
    return SimpleVocabulary([
                    SimpleVocabulary.createTerm('all', 'all', _(u'All Mails')),
                    SimpleVocabulary.createTerm('incoming', 'incoming', _(u'Incoming Mails')),
                    SimpleVocabulary.createTerm('outgoing', 'outgoing', _(u'Outgoing Mails'))])

@grok.provider(IContextSourceBinder)
def possible_categories(context):
    portal = getToolByName(context, 'portal_url').getPortalObject()
    fti = getattr(portal.portal_types, 'courrier_entrant')
    return fti.lookupSchema().get('categorie_de_courrier').value_type.vocabulary

@grok.provider(IContextSourceBinder)
def possible_subcategories(context):
    portal = getToolByName(context, 'portal_url').getPortalObject()
    fti = getattr(portal.portal_types, 'courrier_entrant')
    return fti.lookupSchema().get('sous_categorie_de_courrier').value_type.vocabulary

class IMailSearchForm(interface.Interface):
    text = schema.TextLine(title=_(u'Text'), required=False)
    date = schema.Date(title=_(u'Date'), required=False)
    number = schema.TextLine(title=_(u'Mail Number'), required=False)
    category = schema.Choice(title=_(u'Category'), required=False,
                             source=possible_categories)
    subcategory = schema.Choice(title=_(u'Subcategory'), required=False,
                                source=possible_subcategories)
    scope = schema.Choice(title=_(u'Scope'), required=False,
                          source=possible_scopes, default='all')
    contact = schema.TextLine(title=_(u'Contact'), required=False)

class MailSearchForm(form.Form):
    fields = field.Fields(IMailSearchForm)
    fields['scope'].widgetFactory = FieldRadioboxesWidget
    fields['contact'].widgetFactory = FieldContactWidget
    ignoreContext = True
    template = ViewPageTemplateFile('view_form.pt')

    @button.buttonAndHandler(_(u'Submit'))
    def handleApply(self, action):
        pass


class SearchView(BrowserView):
    batch_macros = ViewPageTemplateFile('batch_macros.pt')

    def form(self):
        f = MailSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def search_results(self):
        f = MailSearchForm(self.context, self.request)
        f.update()

        data, errors = f.extractData()
        kw = {}
        if not data.get('scope'):
            return []

        if data.get('date'):
            kw['mailDate'] = {'query': [data.get('date'), data.get('date')], 'range': 'minmax'}
        if data.get('text'):
            kw['mailSearchableText'] = data.get('text').replace('(', ' ').replace(')', ' ')
        if data.get('number'):
            kw['mailNumber'] = data.get('number')
        if data.get('category'):
            kw['mailCategory'] = data.get('category')
        if data.get('subcategory'):
            kw['mailSubcategory'] = data.get('subcategory')
        if data.get('contact'):
            kw['mailContactFuzzy'] = data.get('contact')

        if data.get('scope') == 'all':
            kw['portal_type'] = ['courrier_entrant', 'courrier_sortant']
        elif data.get('scope') == 'incoming':
            kw['portal_type'] = ['courrier_entrant']
        elif data.get('scope') == 'outgoing':
            kw['portal_type'] = ['courrier_sortant']

        kw['sort_on'] = 'mailDate'
        kw['sort_order'] = 'descending'
        print 'kw:', kw
        catalog = getToolByName(self.context, 'portal_catalog')
        return catalog(**kw)

    def get_batchlinkparams(self):
        d = dict()
        for key in self.request.form:
            d[key] = self.request.form[key]
            if type(d[key]) is str:
                d[key] = unicode(d[key], 'utf-8').encode('utf-8')
            elif type(d[key]) is unicode:
                d[key] = d[key].encode('utf-8')
        return d

    def review_states(self):
        d = {}
        for label, id in self.context.portal_workflow.listWFStatesByTitle():
            d[id] = label
        return d
