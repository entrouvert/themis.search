from five import grok
from zope import interface, schema, component
from zope.interface import implements
from Products.Five import BrowserView
from Products.CMFCore.utils import getToolByName
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from z3c.form import form, field, button, widget
from z3c.form.ptcompat import ViewPageTemplateFile
from z3c.form.browser.radio import RadioWidget
from z3c.form.browser.checkbox import CheckBoxWidget
from z3c.form.browser.text import TextWidget
from z3c.form.interfaces import ITextWidget
import z3c.form.interfaces

from themis.fields.vocabs import get_terms_for_persons
import tabellio.config.utils
from themis.fields import Commission

from interfaces import MessageFactory as _

from mail import FieldContactWidget

def FieldRadioboxesWidget(field, request):
    return widget.FieldWidget(field, RadioWidget(request))

def FieldCheckboxWidget(field, request):
    return widget.FieldWidget(field, CheckBoxWidget(request))

@grok.provider(IContextSourceBinder)
def possible_categories(context):
    catalog = getToolByName(context, 'portal_catalog')
    possible_categories = catalog.uniqueValuesFor('docCategory')
    terms = []
    for category in possible_categories:
        if not category:
            continue
        category_id = category.encode('ascii', 'replace')
        terms.append(SimpleVocabulary.createTerm(category_id, category_id, category))
    terms.sort(cmp_term)
    return SimpleVocabulary(terms)


def cmp_term(x, y):
   from plone.i18n.normalizer.fr import normalizer
   return cmp(normalizer.normalize(x.title), normalizer.normalize(y.title))

@grok.provider(IContextSourceBinder)
def possible_topics(context):
    catalog = getToolByName(context, 'portal_catalog')
    topics = tabellio.config.utils.get_topics_dict()
    terms = []
    for key, value in sorted(topics.items()):
        topic_id = key
        topic_str = value[0]
        terms.append(SimpleVocabulary.createTerm(topic_id, topic_id, topic_str))
    return SimpleVocabulary(terms)


@grok.provider(IContextSourceBinder)
def possible_sessions(context):
    terms = []
    for term in tabellio.config.utils.get_legisl_and_sessions():
        term_id = term.encode('ascii', 'replace')
        terms.append(SimpleVocabulary.createTerm(term_id, term_id, term))
    return SimpleVocabulary(terms)

@grok.provider(IContextSourceBinder)
def possible_status(context):
    return SimpleVocabulary([
                    SimpleVocabulary.createTerm('all', 'all', _(u'All')),
                    SimpleVocabulary.createTerm('private', 'private', _(u'Private')),
                    SimpleVocabulary.createTerm('pending', 'pending', _(u'Pending')),
                    SimpleVocabulary.createTerm('published', 'published', _(u'Public'))])

@grok.provider(IContextSourceBinder)
def possible_commission_status(context):
    portal = getToolByName(context, 'portal_url').getPortalObject()
    fti = getattr(portal.portal_types, 'ProjetD')
    return fti.lookupSchema().get('etat_en_commission').vocabulary


class IDocSearchForm(interface.Interface):
    text = schema.TextLine(title=_(u'Text'), required=False)
    number = schema.TextLine(title=_(u'Document Number'), required=False)
    category = schema.Choice(title=_(u'Category'), required=False,
                             source=possible_categories)
    topics = schema.List(title=_(u'Topics'), required=False,
                    value_type=schema.Choice(required=False,
                    source=possible_topics));
    person = schema.TextLine(title=_(u'Person'), required=False)
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    meeting_date_min = schema.Date(title=_(u'Meeting Date (minimum)'), required=False)
    meeting_date_max = schema.Date(title=_(u'Meeting Date (maximum)'), required=False)
    status = schema.Choice(title=_(u'Status'), required=True, default='all',
                           source=possible_status)
    commission = Commission(title=_(u'Commission'), required=False)
    commission_status = schema.Choice(title=_(u'Commission Status'),
                    required=False, source=possible_commission_status)

class DocSearchForm(form.Form):
    fields = field.Fields(IDocSearchForm)
    fields['topics'].widgetFactory = FieldCheckboxWidget
    fields['person'].widgetFactory = FieldContactWidget
    ignoreContext = True
    template = ViewPageTemplateFile('view_form.pt')

    @button.buttonAndHandler(_(u'Submit'))
    def handleApply(self, action):
        pass


class SearchView(BrowserView):
    batch_macros = ViewPageTemplateFile('batch_macros.pt')

    def form(self):
        f = DocSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def search_results(self):
        f = DocSearchForm(self.context, self.request)
        f.update()

        if not self.request.form.get('form.buttons.submit'):
            return []
        data, errors = f.extractData()
        kw = {}

        if data.get('meeting_date_min') and data.get('meeting_date_max'):
            kw['docMeetingDate'] = {'query': [data.get('meeting_date_min'),
                                              data.get('meeting_date_max')],
                                    'range': 'minmax'}
        elif data.get('meeting_date_min'):
            kw['docMeetingDate'] = {'query': data.get('meeting_date_min'), 'range': 'min'}
        elif data.get('meeting_date_max'):
            kw['docMeetingDate'] = {'query': data.get('meeting_date_max'), 'range': 'max'}

        if data.get('text'):
            kw['docSearchableText'] = data.get('text').replace('(', ' ').replace(')', ' ')
        if data.get('number'):
            kw['docNumber'] = data.get('number')
        if data.get('category'):
            kw['docCategory'] = possible_categories(self.context).by_token.get(data.get('category')).title
        else:
            portal = getToolByName(self.context, 'portal_url').getPortalObject()
            kw['portal_type'] = []
            for typeid in portal.portal_types.objectIds():
                if '(D)' in getattr(portal.portal_types, typeid).Title():
                    kw['portal_type'].append(typeid)
        if data.get('person'):
            kw['docPersonsFuzzy'] = data.get('person')

        if data.get('session'):
            kw['session'] = tabellio.config.utils.get_list_of_sessions(data.get('session'))

        if data.get('topics'):
            kw['docTopics'] = data.get('topics')

        if data.get('commission'):
            kw['docCommissions'] = data.get('commission')

        if data.get('commission_status'):
            kw['docCommissionState'] = data.get('commission_status')

        if data.get('status') and data.get('status') != 'all':
            kw['review_state'] = data.get('status')

        kw['sort_on'] = 'created'
        kw['sort_order'] = 'descending'
        print 'kw:', kw
        catalog = getToolByName(self.context, 'portal_catalog')
        return catalog(**kw)

    def get_batchlinkparams(self):
        d = dict()
        for key in self.request.form:
            d[key] = self.request.form[key]
            if type(d[key]) is str:
                d[key] = unicode(d[key], 'utf-8').encode('utf-8')
            elif type(d[key]) is unicode:
                d[key] = d[key].encode('utf-8')
        return d

    def review_states(self):
        d = {}
        for label, id in self.context.portal_workflow.listWFStatesByTitle():
            d[id] = label
        return d
